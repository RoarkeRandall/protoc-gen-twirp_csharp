package main

import "github.com/golang/protobuf/protoc-gen-go/descriptor"

type message struct {
	proto *descriptor.DescriptorProto
	pkg   string
}

func (m *message) Name() string {
	return m.proto.GetName()
}

func (m *message) FullName() string {
	if m.pkg == "" {

		return m.proto.GetName()
	}

	return m.pkg + "." + m.proto.GetName()
}
