package main

import "github.com/golang/protobuf/protoc-gen-go/descriptor"

type service struct {
	proto     *descriptor.ServiceDescriptorProto
	pkg       string
	namespace string
}

func (svc *service) ClientName() string {
	return svc.proto.GetName() + "Client"
}

func (svc *service) ServiceName() string {
	if svc.pkg == "" {
		return svc.proto.GetName()
	}

	return svc.pkg + "." + svc.proto.GetName()
}

func (svc *service) ServiceNameNoPkg() string {
	return svc.proto.GetName()
}

func (svc *service) Methods() []*method {
	methods := make([]*method, len(svc.proto.GetMethod()))

	for i, proto := range svc.proto.GetMethod() {
		methods[i] = &method{
			proto: proto,
			pkg:   svc.pkg,
		}
	}

	return methods
}

func (svc *service) Namespace() string {
	return svc.namespace
}
