package main

import (
	"bytes"
	"strings"
	"unicode"
)

// camelize converts snake_case to CamelCase
//
// This is a really nieve and probably wrong camelcase implementation
func camelize(input string) string {
	parts := strings.Split(input, ".")

	for i, part := range parts {
		words := strings.Split(part, "_")

		for j, word := range words {
			runed := []rune(word)
			runed[0] = unicode.ToUpper(runed[0])

			words[j] = string(runed)
		}

		parts[i] = strings.Join(words, "")
	}

	return strings.Join(parts, "::")
}

// underscore converts CamelCase to snake_case
func underscore(input string) string {
	buf := &bytes.Buffer{}

	// Iterate over the string. Lower case everything, and add an underscore
	// before any capital that's not the first.
	for i, char := range input {
		if unicode.IsUpper(char) && i > 0 {
			buf.WriteRune('_')
		}

		buf.WriteRune(unicode.ToLower(char))
	}

	return buf.String()
}
