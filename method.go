package main

import (
	"strings"

	"github.com/golang/protobuf/protoc-gen-go/descriptor"
)

type method struct {
	proto *descriptor.MethodDescriptorProto
	pkg   string
}

func (m *method) Name() string {
	return m.proto.GetName()
}

func (m *method) MethodName() string {
	return m.proto.GetName()
}

func (m *method) InputArg() string {
	// name is prefixed with the package
	name := m.proto.GetInputType()
	parts := strings.Split(name, ".")

	return parts[len(parts)-1]
}

func (m *method) OutputType() string {
	// name is prefixed with the package
	name := m.proto.GetOutputType()
	parts := strings.Split(name, ".")

	return parts[len(parts)-1]
}

func (m *method) Deserializer() string {
	// name is prefixed with the package
	name := m.proto.GetOutputType()
	parts := strings.Split(name, ".")

	return parts[len(parts)-1]
}
