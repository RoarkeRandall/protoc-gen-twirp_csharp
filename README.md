# C# twirp client generator

## How to use

1.  Get the plugin

```
go get gitlab.com/RoarkeRandall/protoc-gen-twirp_csharp
```

2.  Generate with the csharp & twirp csharp plugin's enabled:

- add the --csharp_out option to specify where the models will be generated
- add the --twirp_csharp_out option to specify where the client will be generated
- It's recommended to add `option csharp_namespace = "Your.Base.Namespace";` to your proto file's

3.  Add the Client to your IoC Container:

```
services.AddExperimentsServiceClient();
```

4.  Register the config:

```
services.AddOptions<ExperimentsServiceClientConfig>();
```

5.  Add the required references to your project:

- Google.Protobuf
- Newtonsoft.Json
- Microsoft.Extensions.Http
- Microsoft.Extensions.Options.ConfigurationExtensions

## Credits

Forked from github.com/gaffneyc/protoc-gen-twirp_ruby
