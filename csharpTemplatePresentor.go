package main

import "github.com/golang/protobuf/protoc-gen-go/descriptor"

type csharpTemplatePresenter struct {
	proto *descriptor.FileDescriptorProto

	Version        string
	Package        string
	SourceFilename string
}

func (p *csharpTemplatePresenter) ModuleName() string {
	pkg := p.proto.GetPackage()
	if pkg == "" {
		pkg = "Twirp"
	}

	return pkg
}

func (p *csharpTemplatePresenter) Messages() []*message {
	messages := make([]*message, len(p.proto.GetMessageType()))

	for i, msg := range p.proto.GetMessageType() {
		messages[i] = &message{
			proto: msg,
			pkg:   p.proto.GetPackage(),
		}
	}

	return messages
}

func (p *csharpTemplatePresenter) Services() []*service {
	services := make([]*service, len(p.proto.GetService()))

	for i, svc := range p.proto.GetService() {
		namespace := ""
		if p.proto.Options != nil {
			namespace = p.proto.Options.GetCsharpNamespace()
		}
		services[i] = &service{
			proto:     svc,
			pkg:       p.proto.GetPackage(),
			namespace: namespace,
		}
	}

	return services
}
